/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <libmass/IsotopicData.hpp>
#include <QDebug>
#include <QAbstractTableModel>


/////////////////////// Local includes
#include <libmass/Isotope.hpp>
#include "IsotopicDataTableView.hpp"

namespace msxps
{

namespace libmassgui
{

class IsotopicClusterGeneratorDlg;

class IsotopicDataTableViewModel : public QAbstractTableModel
{
  Q_OBJECT

  public:
  IsotopicDataTableViewModel(QObject *object,
                        libmass::IsotopicDataSPtr isotopic_data_sp);
  virtual ~IsotopicDataTableViewModel();

  virtual const QWidget *parent() const;

  void setTableView(IsotopicDataTableView *);
  IsotopicDataTableView *tableView();

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant
  headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;

  bool insertRow(int row, const QModelIndex &parent = QModelIndex());

  QVariant data(const QModelIndex &parent = QModelIndex(),
                int role                  = Qt::DisplayRole) const override;

  bool setData(const QModelIndex &index,
               const QVariant &value,
               int role = Qt::EditRole) override;

  void refreshAfterNewData();
  bool clearAllData();

  Qt::ItemFlags flags(const QModelIndex &index) const override;

  void editCompleted(const QString &text);

  void insertIsotopeRowAbove();
  void insertIsotopeRowBelow();
  void removeSelected();

signals:

  void modifiedSignal();

  private:
  QWidget *mp_parent = nullptr;
  libmass::IsotopicDataSPtr msp_isotopicData;
  IsotopicDataTableView *mp_tableView;

  signals:
  void editCompletedSignal(const QString &text);
};

} // namespace libmassgui

} // namespace msxps

