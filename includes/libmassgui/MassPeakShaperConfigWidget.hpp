/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <libmass/PeakCentroid.hpp>
#include <QDialog>


/////////////////////// libmass includes
#include <libmass/MassPeakShaperConfig.hpp>


/////////////////////// Local includes
#include "ui_MassPeakShaperConfigWidget.h"

namespace msxps
{
namespace libmassgui
{


class MassPeakShaperConfigWidget : public QWidget
{
  Q_OBJECT

  public:
  MassPeakShaperConfigWidget(QWidget *parent_p,
                             libmass::MassPeakShaperConfig &config);

  virtual ~MassPeakShaperConfigWidget();


  void writeSettings(const QString &configSettingsFilePath);
  void readSettings(const QString &configSettingsFilePath);

  void setReferencePeakMz(double mz);

  QSpinBox *getResolutionSpinBox();
  QDoubleSpinBox *getFwhmDoubleSpinBox();
  QSpinBox *getFmwhToBinSizeDivisorSpinBox();
  QSpinBox *getPointCountSpinBox();
  QRadioButton *getGaussianRadioButton();
  QRadioButton *getLorentzianRadioButton();

  double getReferencePeakMz();

  // This function needs to be public because it is necessary to the user of
  // this widget when checking correctness of the parameters.
  bool processBinSizeConfig();

  bool checkParameters();
  bool checkTheParameters(QString &errors);

  public slots:

  void resolutionEditingFinished();
  void fwhmEditingFinished();
  void fwhmBinSizeDivisorValueChanged(int value);
  void pointCountEditingFinished();
  void gaussianRadioButtonToggled(bool checked);
  void lorentzianRadioButtonToggled(bool checked);
  void referencePeakMzEdited(const QString &text);
  void noBinsCheckBoxStateChanged(int state);

  signals:

  void updatedMassPeakShaperConfigSignal(
    const libmass::MassPeakShaperConfig &config);

  private slots:

  private:
  Ui::MassPeakShaperConfigWidget m_ui;
  QWidget *mp_parent = nullptr;

  double m_referencePeakMz = 0.0;

  libmass::MassPeakShaperConfig &m_config;
  double m_normalizingIntensity = std::numeric_limits<double>::min();

  std::shared_ptr<QTimer> msp_msgTimer = std::make_shared<QTimer>();

  void setupWidget();
  void message(const QString &message, int timeout = 3000);
};

} // namespace libmassgui

} // namespace msxps

