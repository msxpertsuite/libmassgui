/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes


/////////////////////// Local includes
#include "ui_MassDataClientServerConfigDlg.h"

namespace msxps
{
namespace libmassgui
{


class MassDataClientServerConfigDlg : public QDialog
{
  Q_OBJECT

  public:
  MassDataClientServerConfigDlg(QWidget *parent,
                                const QString &applicationName,
                                const QString &description);

  virtual ~MassDataClientServerConfigDlg();

  void updateClientConfigurationData(const QString &ip_address, int port_number);
  void clientFailingFeedback(const QString &error);

  void message(const QString &message, int timeout = 3000);

  public slots:

  signals:

  void startServerSignal();
  void startClientSignal(const QString ip_address, int port_number, int connection_frequency);

  void stopServerSignal();
  void stopClientSignal();


  private slots:
  void startClient();

  private:
  Ui::MassDataClientServerConfigDlg m_ui;

  QString m_applicationName;
  QString m_fileName;

  void closeEvent(QCloseEvent *event);

  QWidget *mp_programWindow = nullptr;

  void writeSettings(const QString &configSettingsFilePath);
  void readSettings(const QString &configSettingsFilePath);
};

} // namespace libmassgui

} // namespace msxps

