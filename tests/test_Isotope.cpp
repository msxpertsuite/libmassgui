// ./tests/catch2-tests [section] -s


/////////////////////// Qt includes
#include <QDebug>
#include <QString>


/////////////////////// Catch2 includes
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>


/////////////////////// Local includes
#include <libmass/Isotope.hpp>



TEST_CASE("Isotope test suite", "[Isotope]")
{
  // Set the debugging message formatting pattern.
  qSetMessagePattern(QString("%{file}@%{line}, %{function}(): %{message}"));

  SECTION("..:: Isotope initialization ::..", "[Isotope]")
  {
    bool ok = true;
    REQUIRE(ok == true);
  }
}

