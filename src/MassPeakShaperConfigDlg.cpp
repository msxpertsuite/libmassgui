/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <cmath>
#include <algorithm>
#include <limits> // for std::numeric_limits


/////////////////////// Qt includes
#include <QtGui>
#include <QFileDialog>

/////////////////////// libmass includes
#include <libmass/globals.hpp>
#include <libmass/MassPeakShaperConfig.hpp>
#include <libmass/MassDataCborMassSpectrumHandler.hpp>

/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/processing/filters/filternormalizeintensities.h>

/////////////////////// Local includes
#include "MassPeakShaperConfigDlg.hpp"


namespace msxps
{

namespace libmassgui
{


MassPeakShaperConfigDlg::MassPeakShaperConfigDlg(QWidget *parent_p,
                                                 const QString &applicationName,
                                                 const QString &description)
  : QDialog(static_cast<QWidget *>(parent_p)),
    mp_parent(parent_p),
    m_applicationName{applicationName}
{
  if(!parent_p)
    qFatal("Programming error. Program aborted.");

  m_ui.setupUi(this);

  // Update the window title because the window title element in m_ui might be
  // either erroneous or empty.
  setWindowTitle(QString("%1 - %2").arg(applicationName).arg(description));

  setupDialog();
}


void
MassPeakShaperConfigDlg::closeEvent([[maybe_unused]] QCloseEvent *event)
{
  // qDebug();
  writeSettings(libmass::configSettingsFilePath(m_applicationName));
}


MassPeakShaperConfigDlg::~MassPeakShaperConfigDlg()
{
  // qDebug();

  writeSettings(libmass::configSettingsFilePath(m_applicationName));
}


//! Save the settings to later restore the window in its same position.
void
MassPeakShaperConfigDlg::writeSettings(const QString &configSettingsFilePath)
{
  // qDebug();

  // First save the config widget state.
  mp_massPeakShaperConfigWidget->writeSettings(configSettingsFilePath);

  QSettings settings(configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup("MassPeakShaperConfigDlg");

  settings.setValue("geometry", saveGeometry());

  settings.endGroup();
}


//! Read the settings to restore the window in its last position.
void
MassPeakShaperConfigDlg::readSettings(const QString &configSettingsFilePath)
{
  // qDebug();

  // First read the config widget state.
  mp_massPeakShaperConfigWidget->readSettings(configSettingsFilePath);

  QSettings settings(configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup("MassPeakShaperConfigDlg");

  restoreGeometry(settings.value("geometry").toByteArray());

  settings.endGroup();
}


void
MassPeakShaperConfigDlg::setupDialog()
{

  // We want to destroy the dialog when it is closed.
  // setAttribute(Qt::WA_DeleteOnClose);

  // Setup the peak shaper configuration widget inside of the frame

  QVBoxLayout *v_box_layout_p = new QVBoxLayout(this);

  // Trasmit the config member object reference so that it can be modified by
  // the configuration widget.
  mp_massPeakShaperConfigWidget =
    new MassPeakShaperConfigWidget(this, m_config);
  v_box_layout_p->addWidget(mp_massPeakShaperConfigWidget);

  m_ui.peakShapeConfigFrame->setLayout(v_box_layout_p);

  connect(mp_massPeakShaperConfigWidget,
          &MassPeakShaperConfigWidget::updatedMassPeakShaperConfigSignal,
          [this](const libmass::MassPeakShaperConfig &config) {
            m_config = config;
            //qDebug().noquote() << "Now got the config:" << m_config.toString();
            // Relay the config to the user of this dialog window.
            emit updatedMassPeakShaperConfigSignal(m_config);
          });

  // The values above are actually set in readSettings (with default values if
  // missing in the config settings.)
  readSettings(libmass::configSettingsFilePath(m_applicationName));
}


} // namespace libmassgui

} // namespace msxps
