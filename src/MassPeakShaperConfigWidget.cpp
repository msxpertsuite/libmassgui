/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <cmath>
#include <algorithm>
#include <limits> // for std::numeric_limits


/////////////////////// Qt includes
#include <QPushButton>


/////////////////////// libmass includes
#include <libmass/globals.hpp>
#include <libmass/MassPeakShaperConfig.hpp>

/////////////////////// pappsomspp includes

/////////////////////// Local includes
#include "MassPeakShaperConfigWidget.hpp"


namespace msxps
{

namespace libmassgui
{


MassPeakShaperConfigWidget::MassPeakShaperConfigWidget(
  QWidget *parent_p, libmass::MassPeakShaperConfig &config)
  : QWidget(parent_p), m_config(config)
{
  qDebug() << "Creating MassPeakShaperConfigWidget";
  
  if(!parent_p)
    qFatal("Programming error. Program aborted.");

  setupWidget();
}


MassPeakShaperConfigWidget::~MassPeakShaperConfigWidget()
{
  // The timer should not try to access a deleted widget!
  qDebug() << "Stopping the timer.";
  if(msp_msgTimer.get())
    msp_msgTimer.reset();
 }


void
MassPeakShaperConfigWidget::writeSettings(const QString &configSettingsFilePath)
{
  QSettings settings(configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup("MassPeakShaperConfigWidget");

  settings.setValue("pointCount", m_ui.pointCountSpinBox->value());
  settings.setValue("resolution", m_ui.resolutionSpinBox->value());
  settings.setValue("fwhm", m_ui.fwhmDoubleSpinBox->value());
  settings.setValue("binSize", m_ui.binSizeDoubleSpinBox->value());
  settings.setValue("binSizeGroupBoxState", m_ui.binSizeGroupBox->isChecked());
  settings.setValue("fmwhToBinSizeDivisor",
                    m_ui.fmwhToBinSizeDivisorSpinBox->value());

  settings.endGroup();
}


//! Read the settings to restore the window in its last position.
void
MassPeakShaperConfigWidget::readSettings(const QString &configSettingsFilePath)
{
  // qDebug();

  QSettings settings(configSettingsFilePath, QSettings::IniFormat);
  settings.beginGroup("MassPeakShaperConfigWidget");

  restoreGeometry(settings.value("geometry").toByteArray());
  m_ui.pointCountSpinBox->setValue(settings.value("pointCount", 150).toInt());
  m_ui.resolutionSpinBox->setValue(settings.value("resolution", 45000).toInt());
  m_ui.fwhmDoubleSpinBox->setValue(settings.value("fwhm", 0).toDouble());

  m_ui.binSizeGroupBox->setChecked(
    settings.value("binSizeGroupBoxState", 0).toInt());

  m_ui.binSizeDoubleSpinBox->setValue(settings.value("binSize", 0).toDouble());


  m_ui.fmwhToBinSizeDivisorSpinBox->setValue(
    settings.value("fwhmToBinSizeDivisor", 6).toInt());
  settings.endGroup();
}


void
MassPeakShaperConfigWidget::setReferencePeakMz(double mz)
{
  m_referencePeakMz = mz;
  m_config.setReferencePeakMz(mz);

  m_ui.referenceMassLineEdit->setText(
    QString::number(m_referencePeakMz, 'f', 12));
}


double
MassPeakShaperConfigWidget::getReferencePeakMz()
{
  if(m_referencePeakMz <= 0)
    {
      bool ok = false;

      m_referencePeakMz = m_ui.referenceMassLineEdit->text().toDouble(&ok);

      if(!ok)
        {
          qDebug() << "Failed to convert string to double.";
          return 0;
        }
    }

  return m_referencePeakMz;
}


void
MassPeakShaperConfigWidget::setupWidget()
{
  m_ui.setupUi(this);

  // Ranges for various numerical values
  m_ui.resolutionSpinBox->setRange(0, 2000000);
  m_ui.fwhmDoubleSpinBox->setRange(0, 10);
  m_ui.pointCountSpinBox->setRange(5, 1000);

  // By default we want a gaussian-type shape.
  m_config.setMassPeakShapeType(libmass::MassPeakShapeType::GAUSSIAN);
  m_ui.gaussianRadioButton->setChecked(true);

  // Get the number of points used to craft the peak curve.
  m_config.setPointCount(m_ui.pointCountSpinBox->value());
  
  // Set the message timer to be singleShot. This time is used to erase the text shown in the message line edit widget after 3 seconds.
  msp_msgTimer->setInterval(3000);
  msp_msgTimer->setSingleShot(true);
  connect(msp_msgTimer.get(),
          &QTimer::timeout,
          [this] (){ m_ui.messageLineEdit->setText("");});

  connect(m_ui.referenceMassLineEdit,
          &QLineEdit::textEdited,
          this,
          &MassPeakShaperConfigWidget::referencePeakMzEdited);

  connect(m_ui.fmwhToBinSizeDivisorSpinBox,
          &QSpinBox::valueChanged,
          this,
          &MassPeakShaperConfigWidget::fwhmBinSizeDivisorValueChanged);

  connect(m_ui.resolutionSpinBox,
          &QSpinBox::editingFinished,
          this,
          &MassPeakShaperConfigWidget::resolutionEditingFinished);

  connect(m_ui.fwhmDoubleSpinBox,
          &QSpinBox::editingFinished,
          this,
          &MassPeakShaperConfigWidget::fwhmEditingFinished);

  connect(m_ui.pointCountSpinBox,
          &QSpinBox::editingFinished,
          this,
          &MassPeakShaperConfigWidget::pointCountEditingFinished);

  connect(m_ui.gaussianRadioButton,
          &QRadioButton::toggled,
          this,
          &MassPeakShaperConfigWidget::gaussianRadioButtonToggled);

  connect(m_ui.lorentzianRadioButton,
          &QRadioButton::toggled,
          this,
          &MassPeakShaperConfigWidget::lorentzianRadioButtonToggled);

  connect(m_ui.checkParametersPushButton,
          &QPushButton::clicked,
          this,
          &MassPeakShaperConfigWidget::checkParameters);

  connect(m_ui.noBinsCheckBox,
          &QCheckBox::stateChanged,
          this,
          &MassPeakShaperConfigWidget::noBinsCheckBoxStateChanged);
}


void
MassPeakShaperConfigWidget::noBinsCheckBoxStateChanged(int state)
{
  if(state == Qt::Checked)
    {
      // The user does not want bins to be used.

      m_ui.binSizeLogicGroupBox->setEnabled(false);
      m_ui.binSizeDoubleSpinBox->setEnabled(false);
    }
  else
    {
      m_ui.binSizeLogicGroupBox->setEnabled(true);
      m_ui.binSizeDoubleSpinBox->setEnabled(true);
    }
}


bool
MassPeakShaperConfigWidget::processBinSizeConfig()
{
  // We want to understand what the user is expecting: create bins or not,
  // and do they want to have the bin size configured manually or do they
  // expect that the bin size be determined automatically.

  double bin_size            = 0;
  bool set_bin_size_manually = m_ui.binSizeGroupBox->isChecked();
  bool no_bins               = m_ui.noBinsCheckBox->isChecked();

  bool ok = false;

  // First off, report the usage of bins. If no bins are to be calculated,
  // then it is not necessary to bother with the fwhm / bin_size factor (see
  // below).
  if(no_bins)
    {
      // qDebug() << "The user requests no bins.";

      m_config.setWithBins(false);
      m_config.setBinSize(0);

      return true;
    }
  else
    {
      m_config.setWithBins(true);
      // qDebug() << "The user requests bins, either automatic of manual.";

      if(set_bin_size_manually)
        {
          // We are checking the manual settings configuration.

          // qDebug() << "Configuring the bin size by direct setting.";

          m_config.setBinSizeFixed(true);

          // Since the bin size is set manually, let the config object know that
          // it does not need to perform any computation.
          m_config.setBinSizeDivisor(1);

          // Since the user checked the group box and they did not check the "no
          // bins" checkbox, then that means that they want to
          // actually set the bin size.

          bin_size = m_ui.binSizeDoubleSpinBox->value();

          if(!bin_size)
            {
              qDebug() << "The bin size cannot be 0.";
              message("The bin size cannot be 0.");
              return false;
            }

          m_config.setBinSize(bin_size);

          // qDebug() << "Bin size set manually:" << bin_size;
        }
      else
        {
          // qDebug() << "Configuring the bin size by calculation.";

          // The user did not elect to set the bin size manually, we can
          // compute it and update the value in its spin box.

          // The mass peak shaper will compute the bin size simply by dividing
          // FWHM by that factor. The idea is that it takes roughly that value
          // data points to craft a peak that has that FWHM.
          int divisor = m_ui.fmwhToBinSizeDivisorSpinBox->value();

          if(!divisor)
            {
              qDebug() << "The FWHM / bin size divisor cannot be 0.";
              message(
                "The FWHM / bin size divisor cannot be 0. Please, fix it.");

              return false;
            }

          // That divisor will be accounted for in the config when
          // asked to compute the bin size = fwhm / divisor. An empirically good
          // value is 6.
          m_config.setBinSizeDivisor(divisor);
          // qDebug() << "Set bin a size divisor of" << ratio;

          m_config.setBinSizeFixed(false);

          double bin_size = m_config.binSize(&ok);

          if(!ok)
            {
              message("Could not compute the bin size.");
              qDebug() << "Could not compute the bin size.";
              return false;
            }

          m_ui.binSizeDoubleSpinBox->setValue(bin_size);

          //qDebug() << "The configuration has computed bin size:" << bin_size;
        }
    }

  return true;
}


void
MassPeakShaperConfigWidget::resolutionEditingFinished()
{
  double resolution = m_ui.resolutionSpinBox->value();

  if(!resolution)
    {
      // Tell the user to set a valid FWHM value, then.
      message("Will use the FWHM. Please, set a valid FWHM value");

      return;
    }

  // At this point we know that resolution contains a proper value.

  m_config.setResolution(resolution);

  // We can compute the FWHM using the resolution only if the reference peaks's
  // m/z value has been set.

  if(m_config.getReferencePeakMz() <= 0)
    {
      message("Please fill-in the reference peak's m/z value.");

      return;
    }

  QString msg;

  msg += "Will try to use the resolution... ";

  message(msg);

  bool ok = false;

  double fwhm = m_config.fwhm(&ok);

  if(!ok)
    {
      msg += "Failed. Check the parameters.";
      message(msg);
      return;
    }

  m_ui.fwhmDoubleSpinBox->setValue(fwhm);

  // Check if we have enough of data to actually compute the bin size (or not
  // depending on the user's requirements).

  ok = processBinSizeConfig();

  if(ok && m_config.withBins())
    {
      QString msg = QString("Could compute the bin size: %1")
                      .arg(m_config.getBinSize(), 0, 'f', 10);

      message(msg);

      // Since we used the resolution as the starting point, show that fact.
      m_ui.resolutionBasedRadioButton->setChecked(true);
    }

  return;
}


void
MassPeakShaperConfigWidget::fwhmEditingFinished()
{
  double fwhm = m_ui.fwhmDoubleSpinBox->value();

  if(!fwhm)
    {
      // Tell the user to set a valid resolution value, then.
      message("Will use the resolution. Please, set a valid resolution value");
      return;
    }

  // At this point we know that fwhm contains a proper value.

  m_config.setFwhm(fwhm);

  // We can compute the resolution using FWHM only if the reference peaks's m/z
  // value has been set.

  if(m_config.getReferencePeakMz() <= 0)
    {
      message("Please fill-in the reference peak's m/z value.");

      return;
    }

  QString msg;

  msg += "Will use the FWHM... ";

  message(msg);

  bool ok = false;

  double resolution = m_config.resolution(&ok);

  // This is a peculiar situation, because from here we MUST be able to
  // compute the resolution, because all that is required is the FWHM, that we
  // have, and the reference peak centroid that SHOULD be there also. So, make
  // the error fatal.

  if(!ok)
    qFatal(
      "Programming error. At this point we should be able to compute the "
      "resolution.");

  m_ui.resolutionSpinBox->setValue(resolution);

  // Check if we have enough of data to actually compute the bin size (or not
  // depending on the user's requirements).

  ok = processBinSizeConfig();

  if(ok && m_config.withBins())
    {
      QString msg = QString("Could compute the bin size: %1")
                      .arg(m_config.getBinSize(), 0, 'f', 10);

      // Since we used the FWHM as the starting point, show that fact.
      m_ui.fwhmBasedRadioButton->setChecked(true);

      message(msg);
    }

  return;
}


void
MassPeakShaperConfigWidget::pointCountEditingFinished()
{
  // The points have changed, we'll use that value to compute the m/z step
  // between two consecutive points in the shaped peak.

  // All we need is either FWHM or resolution to advance the configuration.
  // Just check what we have.

  int point_count = m_ui.pointCountSpinBox->value();

  if(point_count < 5)
    {
      message("The number of points to craft the shape is too small.");
      return;
    }

  m_config.setPointCount(point_count);

  // At this point try to perform some calculations.

  double fwhm    = m_ui.fwhmDoubleSpinBox->value();
  int resolution = m_ui.resolutionSpinBox->value();

  if(fwhm)
    fwhmEditingFinished();
  else if(resolution)
    resolutionEditingFinished();

  // The bin size configuration is handled by the functions above.

  // That's all we can do.
}

void
MassPeakShaperConfigWidget::referencePeakMzEdited(const QString &text)
{
  bool ok = false;

  double mz = text.toDouble(&ok);

  if(!ok)
    {
      message("Please fix the reference peaks' m/z value.");
      return;
    }

  m_config.setReferencePeakMz(mz);
}


void
MassPeakShaperConfigWidget::fwhmBinSizeDivisorValueChanged(
  [[maybe_unused]] int value)
{
  // Recalculate all the bin size stuff.
  processBinSizeConfig();
}


void
MassPeakShaperConfigWidget::gaussianRadioButtonToggled(bool checked)
{
  if(checked)
    m_config.setMassPeakShapeType(libmass::MassPeakShapeType::GAUSSIAN);
  else
    m_config.setMassPeakShapeType(libmass::MassPeakShapeType::LORENTZIAN);
}


void
MassPeakShaperConfigWidget::lorentzianRadioButtonToggled(bool checked)
{
  if(checked)
    m_config.setMassPeakShapeType(libmass::MassPeakShapeType::LORENTZIAN);
  else
    m_config.setMassPeakShapeType(libmass::MassPeakShapeType::GAUSSIAN);
}


void
MassPeakShaperConfigWidget::message(const QString &message, int timeout)
{
  m_ui.messageLineEdit->setText(message);
  msp_msgTimer->stop();
  msp_msgTimer->setInterval(timeout);
  msp_msgTimer->start();
}


QSpinBox *
MassPeakShaperConfigWidget::getResolutionSpinBox()
{
  return m_ui.resolutionSpinBox;
}


QDoubleSpinBox *
MassPeakShaperConfigWidget::getFwhmDoubleSpinBox()
{
  return m_ui.fwhmDoubleSpinBox;
}


QSpinBox *
MassPeakShaperConfigWidget::getFmwhToBinSizeDivisorSpinBox()
{
  return m_ui.fmwhToBinSizeDivisorSpinBox;
}


QSpinBox *
MassPeakShaperConfigWidget::getPointCountSpinBox()
{
  return m_ui.pointCountSpinBox;
}


QRadioButton *
MassPeakShaperConfigWidget::getGaussianRadioButton()
{
  return m_ui.gaussianRadioButton;
}


QRadioButton *
MassPeakShaperConfigWidget::getLorentzianRadioButton()
{
  return m_ui.lorentzianRadioButton;
}


// Call back for the push button
bool
MassPeakShaperConfigWidget::checkParameters()
{
  QString errors;
  return checkTheParameters(errors);
}


// Function that can be called from outside.
bool
MassPeakShaperConfigWidget::checkTheParameters(QString &errors)
{
  m_config.reset();

  // The general idea is that a number of parameters are inter-dependent. We
  // need to check these parameters in a precise order because of these
  // inter-dependencies.

  bool ok           = false;
  m_referencePeakMz = QString(m_ui.referenceMassLineEdit->text()).toDouble(&ok);

  if(!ok || m_referencePeakMz <= 0)
    {
      QString msg("Please fill in the reference peak m/z value.");

      message(msg);

      errors = msg;

      qDebug() << msg;

      return false;
    }

  // We need to set back the reference peak centroid because the m_config was
  // reset above.
  m_config.setReferencePeakMz(m_referencePeakMz);

  // We will need this value for computations below.
  int point_count = m_ui.pointCountSpinBox->value();

  if(point_count < 3)
    {
      QString msg(
        "The number of points allowed to craft the shape is too small. ");

      message(msg);

      errors = msg;

      qDebug() << msg;

      return false;
    }

  m_config.setPointCount(point_count);
  // qDebug() << "The set point count:" << m_config.getPointCount();

  // Get to know if we want gaussian or lorentzian shapes.
  if(m_ui.gaussianRadioButton->isChecked())
    {
      m_config.setMassPeakShapeType(libmass::MassPeakShapeType::GAUSSIAN);
    }
  else
    {
      m_config.setMassPeakShapeType(libmass::MassPeakShapeType::LORENTZIAN);
    }

  // Now check the resolution/fwhm data that are competing one another.

  double fwhm    = m_ui.fwhmDoubleSpinBox->value();
  int resolution = m_ui.resolutionSpinBox->value();

  // Both values cannot be naught.
  if(!resolution && !fwhm)
    {
      QString msg(
        "Please, fix the Resolution / FWHM value (one or the other).");

      message(msg);

      errors = msg;

      qDebug() << msg;

      // By default we favor the resolving power-based calculation.
      m_ui.resolutionBasedRadioButton->setChecked(true);

      return false;
    }

  if(resolution)
    {
      // We will use the resolution
      m_config.setResolution(resolution);

      // We have to compute the FWHM value because that is a fundamental
      // parameter for the peak shaping. We use the most intense peak
      // centroid's mz value for that.

      bool ok = false;

      // With all the data successfully tested above, we must be able to
      // compute the FWHM. The config will store the computed value.
      double fwhm = m_config.fwhm(&ok);

      if(!ok)
        {
          QString msg("Failed to compute FWHM starting from resolution.");

          message(msg);

          errors = msg;

          qDebug() << msg;

          // By default we favor the resolving power-based calculation.
          m_ui.resolutionBasedRadioButton->setChecked(true);

          return false;
        }

      m_ui.fwhmDoubleSpinBox->setValue(fwhm);

      if(m_config.getMassPeakWidthLogic() ==
         libmass::MassPeakWidthLogic::RESOLUTION)
        m_ui.resolutionBasedRadioButton->setChecked(true);
      else if(m_config.getMassPeakWidthLogic() ==
              libmass::MassPeakWidthLogic::FWHM)
        m_ui.fwhmBasedRadioButton->setChecked(true);
    }
  // End of
  // if(resolution)
  else
    {
      // We will use the FWHM
      m_config.setFwhm(fwhm);

      // We all the data above, we should be able to compute the resolution
      // and set it to the config.

      bool ok    = false;
      resolution = m_config.resolution(&ok);

      if(!ok)
        {
          QString msg("Failed to compute the resolution.");

          message(msg);

          errors = msg;

          qDebug() << msg;

          // By default we favor the resolving power-based calculation.
          m_ui.resolutionBasedRadioButton->setChecked(true);

          return false;
        }

      m_ui.resolutionSpinBox->setValue(resolution);

      if(m_config.getMassPeakWidthLogic() ==
         libmass::MassPeakWidthLogic::RESOLUTION)
        m_ui.resolutionBasedRadioButton->setChecked(true);
      else if(m_config.getMassPeakWidthLogic() ==
              libmass::MassPeakWidthLogic::FWHM)
        m_ui.fwhmBasedRadioButton->setChecked(true);
    }
  // End of
  // ! if(resolution), that is use FWHM

  //////////////////// THE BIN SIZE /////////////////////

  // qDebug() << "Now processing the bin size configuration.";

  if(!processBinSizeConfig())
    {
      QString msg("Failed to compute the bin size.");

      message(msg);

      errors = msg;

      qDebug() << msg;

      return false;
    }

  // Craft a string describing the whole set of params as we have been
  // working on them in the configuration instance.

  QString info = m_config.toString();

  //qDebug().noquote() << "Configuration:" << info;

  if(!m_config.resolve())
    {
      QString msg("Failed to finally resolve the configuration.");

      message(msg);

      errors = msg;

      qDebug() << msg;

      return false;
    }

  //qDebug() << "Emitting signal with the configuration.";

  emit updatedMassPeakShaperConfigSignal(m_config);

  message("The parameters were validated successfully.");

  return true;
}


} // namespace libmassgui

} // namespace msxps

