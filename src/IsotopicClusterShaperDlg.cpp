/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <cmath>
#include <algorithm>
#include <limits> // for std::numeric_limits


/////////////////////// Qt includes
#include <QtGui>
#include <QMessageBox>
#include <QFileDialog>

/////////////////////// libmass includes
#include <libmass/globals.hpp>
#include <libmass/MassDataCborMassSpectrumHandler.hpp>
#include <libmass/IsotopicClusterGenerator.hpp>
#include <libmass/MassPeakShaper.hpp>
#include <libmass/MassPeakShaperConfig.hpp>
#include <libmass/PeakCentroid.hpp>



/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/processing/filters/filternormalizeintensities.h>

/////////////////////// Local includes
#include "IsotopicClusterShaperDlg.hpp"
#include "ColorSelector.hpp"



namespace msxps
{

namespace libmassgui
{


IsotopicClusterShaperDlg::IsotopicClusterShaperDlg(
  QWidget *program_window_p,
  const QString &applicationName,
  const QString &description)
  : QDialog(static_cast<QWidget *>(program_window_p)),
    mp_programWindow(program_window_p),
    m_applicationName(applicationName),
    m_description(description)
{

  if(!program_window_p)
    qFatal("Programming error. Program aborted.");

  setupDialog();

  // Update the window title because the window title element in m_ui might be
  // either erroneous or empty.
  setWindowTitle(QString("%1 - %2").arg(applicationName).arg(description));
}


IsotopicClusterShaperDlg::IsotopicClusterShaperDlg(
  QWidget *program_window_p,
  const QString &applicationName,
  const QString &description,
  pappso::TraceCstSPtr isotopic_cluster_sp,
  int normalizing_intensity)
  : QDialog(static_cast<QWidget *>(program_window_p)),
    mp_programWindow(program_window_p),
    m_applicationName(applicationName),
    m_description(description)
{
  if(!program_window_p)
    qFatal("Programming error. Program aborted.");

  setupDialog();

  // Update the window title because the window title element in m_ui might be
  // either erroneous or empty.
  setWindowTitle(QString("%1 - %2").arg(applicationName).arg(description));

  m_normalizingIntensity = normalizing_intensity;
  m_ui.normalizingIntensitySpinBox->setValue(normalizing_intensity);

  // Logically if setting the normalizing intensity, then activate the group
  // box.
  m_ui.normalizationGroupBox->setChecked(true);

  setIsotopiCluster(isotopic_cluster_sp);
}


void
IsotopicClusterShaperDlg::closeEvent([[maybe_unused]] QCloseEvent *event)
{
  // qDebug();
  writeSettings(libmass::configSettingsFilePath(m_applicationName));
}


IsotopicClusterShaperDlg::~IsotopicClusterShaperDlg()
{
  // qDebug();

  writeSettings(libmass::configSettingsFilePath(m_applicationName));
}


//! Save the settings to later restore the window in its same position.
void
IsotopicClusterShaperDlg::writeSettings(const QString &configSettingsFilePath)
{
  // qDebug();

  // First save the config widget state.
  mp_massPeakShaperConfigWidget->writeSettings(configSettingsFilePath);

  QSettings settings(configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup("IsotopicClusterShaperDlg");

  settings.setValue("geometry", saveGeometry());
  settings.setValue("charge", m_ui.ionChargeSpinBox->value());
  settings.setValue("splitter", m_ui.splitter->saveState());

  settings.endGroup();
}


//! Read the settings to restore the window in its last position.
void
IsotopicClusterShaperDlg::readSettings(const QString &configSettingsFilePath)
{
  // qDebug();

  // First read the config widget state.
  mp_massPeakShaperConfigWidget->readSettings(configSettingsFilePath);

  QSettings settings(configSettingsFilePath, QSettings::IniFormat);

  settings.beginGroup("IsotopicClusterShaperDlg");

  restoreGeometry(settings.value("geometry").toByteArray());
  m_ui.ionChargeSpinBox->setValue(settings.value("charge", 1).toInt());

  m_ui.splitter->restoreState(settings.value("splitter").toByteArray());

  settings.endGroup();
}


void
IsotopicClusterShaperDlg::setupDialog()
{
  m_ui.setupUi(this);

  // We want to destroy the dialog when it is closed.
  setAttribute(Qt::WA_DeleteOnClose);

  // Setup the peak shaper configuration widget inside of the frame

  QVBoxLayout *v_box_layout_p = new QVBoxLayout(this);

  // Trasmit the config member object reference so that it can be modified by
  // the configuration widget.
  mp_massPeakShaperConfigWidget =
    new MassPeakShaperConfigWidget(this, m_config);
  v_box_layout_p->addWidget(mp_massPeakShaperConfigWidget);

  m_ui.peakShapeConfigFrame->setLayout(v_box_layout_p);

  // Set the message timer to be singleShot. This time is used to erase the text shown in the message line edit widget after 3 seconds.
  msp_msgTimer->setInterval(3000);
  msp_msgTimer->setSingleShot(true);
  connect(msp_msgTimer.get(),
          &QTimer::timeout,
          [this] (){ m_ui.messageLineEdit->setText("");});

  connect(m_ui.normalizationGroupBox,
          &QGroupBox::toggled,
          this,
          &IsotopicClusterShaperDlg::normalizingGrouBoxToggled);

  connect(m_ui.normalizingIntensitySpinBox,
          &QSpinBox::valueChanged,
          this,
          &IsotopicClusterShaperDlg::normalizingIntensityValueChanged);


  // Set the default color of the mass spectra trace upon mass spectrum
  // synthesis
  QColor color("black");
  // Now prepare the color in the form of a QByteArray
  QDataStream stream(&m_colorByteArray, QIODevice::WriteOnly);
  stream << color;


  // The color button
  connect(m_ui.colorSelectorPushButton,
          &QPushButton::clicked,
          this,
          &IsotopicClusterShaperDlg::traceColorPushButtonClicked);

  // The values above are actually set in readSettings (with default values if
  // missing in the config settings.)
  readSettings(libmass::configSettingsFilePath(m_applicationName));


  // Always start the dialog with the first page of the tab widget,
  // the input data page.
  m_ui.tabWidget->setCurrentIndex(static_cast<int>(TabWidgetPage::INPUT_DATA));

  // Throughout of *this* dialog window, the normalization factor
  // for the peak shapes (Gaussian, specifically) is going to be 1.

  connect(m_ui.importPushButton,
          &QPushButton::clicked,
          this,
          &IsotopicClusterShaperDlg::importFromText);

  connect(m_ui.checkParametersPushButton,
          &QPushButton::clicked,
          this,
          &IsotopicClusterShaperDlg::checkParameters);

  connect(m_ui.runPushButton,
          &QPushButton::clicked,
          this,
          &IsotopicClusterShaperDlg::run);

  connect(m_ui.outputFilePushButton,
          &QPushButton::clicked,
          this,
          &IsotopicClusterShaperDlg::outputFileName);

  connect(m_ui.displayMassSpectrumPushButton,
          &QPushButton::clicked,
          this,
          &IsotopicClusterShaperDlg::displayMassSpectrum);

  connect(m_ui.copyMassSpectrumToClipboardPushButton,
          &QPushButton::clicked,
          this,
          &IsotopicClusterShaperDlg::copyMassSpectrumToClipboard);
}


QString
IsotopicClusterShaperDlg::craftMassSpectrumName()
{
  // The user might have forgotten to insert a preferred name in the mass
  // spectrum file name edit widget. We thus craft one on the basis of the
  // centroid peaks and the current time.

  QString name = m_ui.massSpectrumNameResultsLineEdit->text();

  if(name.isEmpty())
    {
      name =
        QString("%1-%2 @ %3")
          .arg(m_peakShapers.at(0)->getPeakCentroid().x)
          .arg(m_peakShapers.at(m_peakShapers.size() - 1)->getPeakCentroid().x)
          .arg(QTime::currentTime().toString("hh:mm:ss.zzz"));
    }
  else
    {
      name.append(" @ ");
      name.append(QTime::currentTime().toString("hh:mm:ss.zzz"));
    }

  // qDebug() << "Returning mass spectrum name:" << name;
  return name;
}


std::size_t
IsotopicClusterShaperDlg::fillInThePeakShapers()
{
  // qDebug();

  // Clear the peak shapers that might have been created in a previous run.
  m_peakShapers.clear();

  int charge = m_ui.ionChargeSpinBox->value();
  // We compute the m/z value below, so z cannot be 0.
  if(charge <= 0)
    qFatal(
      "Programming error. By this time the charge should have been validated > "
      "0");

  for(auto data_point : m_isotopicCluster)
    {
      // We need to account for the charge. Note the division below.

      m_peakShapers.append(std::make_shared<libmass::MassPeakShaper>(
        pappso::DataPoint(data_point.x / charge, data_point.y), m_config));
    }

  // qDebug() << "m_config:" << m_config.asText(800);

  message(
    QString("The number of peak centroids is: %1").arg(m_peakShapers.size()));

  return m_peakShapers.size();
}


bool
IsotopicClusterShaperDlg::checkParameters()
{
  qDebug();

  // The general idea is that a number of parameters are inter-dependent. We
  // need to check these parameters in a precise order because of these
  // inter-dependencies.

  // Remove all the text in the log text edit widget.
  m_ui.logPlainTextEdit->clear();

  // Remove all text in the results text edit widget.
  m_ui.resultPlainTextEdit->clear();

  // Make sure we have some (mz,i) values to crunch.
  if(!m_isotopicCluster.size())
    {
      message("There are no centroid peaks to process");
      qDebug("There are no centroid peaks to process");

      return false;
    }

  if(m_referencePeakMz <= 0)
    {
      message(
        "The reference peak m/z value is not set. Please enter one m/z value.");
      qDebug() << "The reference peak m/z value is not set";

      return false;
    }

  //////////////////// THE ION CHARGE /////////////////////

  int charge = m_ui.ionChargeSpinBox->value();
  // m/z has charge at the denominator!
  if(charge <= 0)
    {
      message("The ion charge must be >= 1", 6000);
      return false;
    }

  /////////////////// Ask the widget to perform the check ///////////////////

  QString text;

  if(!mp_massPeakShaperConfigWidget->checkTheParameters(text))
    {
      message("There were errors in the configuration: " + text, 10000);
      return false;
    }

  // No errors ? Then output the configuration.

  text = m_config.toString();

  qDebug().noquote() << "The configuration:\n" << text;

  m_ui.logPlainTextEdit->appendPlainText(text);

  // Finally do a resolve of all the config bits to something actually useful.

  if(!m_config.resolve())
    {
      message(
        "Failed to finally resolve the configuration. Check the LOG tab for "
        "details.");
      qDebug().noquote() << text;

      return false;
    }

  // At this point, because we have set all the relevant values to the
  // m_config PeakShapeConfig, we can create the PeakShaper instances and set
  // to them the m_config.

  if(!fillInThePeakShapers())
    {
      QMessageBox::warning(
        0,
        tr("mineXpert: Peak shaper (Gaussian or Lorentzian)"),
        tr("Please, insert at least one (m/z i) pair in the following "
           "format:\n\n"
           "<number><separator><number>\n\n"
           "With <separator> being any non numerical character \n"
           "(space or non-'.' punctuation, for example).\n\n"
           "Once the data have been pasted in the edit widget, click the "
           "'Import from text' button above.\n"
           "Also, make sure that you fill-in the resolution or the FWHM "
           "value and the ion charge."),
        QMessageBox::Ok);

      return false;
    }

  m_ui.logPlainTextEdit->appendPlainText(
    QString("Number of input peak centroids to process: %1.\n")
      .arg(m_peakShapers.size()));

  m_ui.logPlainTextEdit->appendPlainText(m_config.toString());


  message("The configuration validated fine. Check the LOG tab for details.");

  // At this point we'll have some things to report to the LOG tab,
  // switch to it now.
  // m_ui.tabWidget->setCurrentIndex(static_cast<int>(TabWidgetPage::LOG));

  // qDebug() << "Now returning true after having checked the parameters.";
  return true;
}


void
IsotopicClusterShaperDlg::message(const QString &message, int timeout)
{
  m_ui.messageLineEdit->setText(message);
  msp_msgTimer->stop();
  msp_msgTimer->setInterval(timeout);
  msp_msgTimer->start();
}


void
IsotopicClusterShaperDlg::run()
{
  qDebug();

  if(!checkParameters())
    {
      QMessageBox::warning(
        0,
        tr("mineXpert: Peak shaper (Gaussian or Lorentzian)"),
        tr("Please, insert at least one (m/z i) pair in the following "
           "format:\n\n"
           "<number><separator><number>\n\n"
           "With <separator> being any non numerical character \n"
           "(space or non-'.' punctuation, for example).\n\n"
           "Once the data have been pasted in the edit widget, click the "
           "'Import from text' button above.\n"
           "Also, make sure that you fill-in the resolution or the FWHM "
           "value and the ion charge."),
        QMessageBox::Ok);

      return;
    }

  // At this point all the data are set, we can start the computation on all
  // the various peak shapers.

  double minMz = std::numeric_limits<double>::max();
  double maxMz = std::numeric_limits<double>::min();

  // Clear the map trace that will receive the results of the combinations.
  m_mapTrace.clear();

  // Now actually shape a peak around each peak centroid (contained in each
  // peak shaper).

  int processed = 0;

  for(auto peak_shaper_sp : m_peakShapers)
    {
      if(!peak_shaper_sp->computePeakShape())
        {
          QMessageBox::warning(
            0,
            tr("mineXpert: Peak shaper (Gaussian or Lorentzian)"),
            QString("Failed to compute a Trace for peak shape at m/z %1.")
              .arg(peak_shaper_sp->getPeakCentroid().x, QMessageBox::Ok));

          return;
        }

      // Now that we have computed the full shape around the centroid, we'll
      // be able to extract the smallest and greatest mz values of the whole
      // shape. We will need these two bounds to craft the bins in the
      // MzIntegrationParams object.

      if(peak_shaper_sp->getTrace().size())
        {
          double smallestMz = peak_shaper_sp->getTrace().front().x;
          minMz             = std::min(smallestMz, minMz);

          double greatestMz = peak_shaper_sp->getTrace().back().x;
          maxMz             = std::max(greatestMz, maxMz);
        }

      ++processed;
    }

  // Now create the actual spectrum by combining all the shapes into a cluster
  // shape.

  if(m_config.withBins())
    {
      // Bins were requested.

      // There are two situations:
      // 1. The bin size is fixed.
      // 2. The bin size is dynamically calculated on the basis of the
      // resolution and of the bin size divisor.

      double bin_size = m_config.getBinSize();

      if(m_config.getBinSizeFixed())
        {
          // The bin size is fixed, easy situation.

          qDebug() << "The bin size is FIXED.";

          m_mzIntegrationParams = pappso::MzIntegrationParams(
            minMz,
            maxMz,
            pappso::BinningType::ARBITRARY,
            -1,
            pappso::PrecisionFactory::getResInstance(bin_size),
            1, // bin size divisor, 1 = no-op
            true);
        }
      else
        {
          // The bin size is dynamically calculated.

          if(m_config.getMassPeakWidthLogic() ==
             libmass::MassPeakWidthLogic::FWHM)
            {
              qDebug() << "The mass peak width logic is FWHM.";

              m_mzIntegrationParams = pappso::MzIntegrationParams(
                minMz,
                maxMz,
                pappso::BinningType::ARBITRARY,
                -1,
                pappso::PrecisionFactory::getDaltonInstance(bin_size),
                1,
                true);
            }
          else if(m_config.getMassPeakWidthLogic() ==
                  libmass::MassPeakWidthLogic::RESOLUTION)
            {
              qDebug() << "The mass peak width logic is RESOLUTION.";
              m_mzIntegrationParams = pappso::MzIntegrationParams(
                minMz,
                maxMz,
                pappso::BinningType::ARBITRARY,
                -1,
                pappso::PrecisionFactory::getResInstance(
                  m_config.getResolution()),
                m_config.getBinSizeDivisor(),
                true);
            }
          else
            qFatal(
              "Programming error. At this point, the peak width logic should "
              "have been set.");
        }


      // Now compute the bins.
      std::vector<double> bins = m_mzIntegrationParams.createBins();

      // We will need to perform combinations, positive combinations.
      pappso::MassSpectrumPlusCombiner mass_spectrum_plus_combiner;

      mass_spectrum_plus_combiner.setBins(bins);

      for(auto mass_peak_shaper_sp : m_peakShapers)
        {
          mass_spectrum_plus_combiner.combine(m_mapTrace,
                                              mass_peak_shaper_sp->getTrace());
        }
    }
  else
    {
      // No bins were required. We can combine simply:

      qDebug() << "NO BINS requested.";

      pappso::TracePlusCombiner trace_plus_combiner(-1);

      for(auto mass_peak_shaper_sp : m_peakShapers)
        {
          // The combiner does not handle any data point for which y = 0.
          // So the result trace does not have a single such point;
          trace_plus_combiner.combine(m_mapTrace,
                                      mass_peak_shaper_sp->getTrace());
        }
    }

  message(QString("Successfully processed %1 peak centroids").arg(processed));

  // This is actually where the normalization needs to be performed. The
  // user might have asked that the apex of the shape be at a given
  // intensity. This is actually true when the IsoSpec++-based calculations
  // have been performed, that is the peak centroids have the expected
  // intensities normalized to the expected value. But then we have binned
  // all the numerous peak centroids into bins that are way larger than the
  // mz_step above. This binning is crucial to have a final spectrum that
  // actually looks like a real one. But it comes with a drawback: the
  // intensities of the m/z bins are no more the one of the initial
  // centroids, they are way larger. So we need to normalize the obtained
  // trace.

  m_finalTrace.clear();

  if(m_ui.normalizationGroupBox->isChecked())
    {
      m_normalizingIntensity = m_ui.normalizingIntensitySpinBox->value();

      // qDebug() << "Now normalizing to  intensity = " <<
      // m_normalizingIntensity;

      pappso::Trace trace = m_mapTrace.toTrace();
      m_finalTrace        = trace.filter(
        pappso::FilterNormalizeIntensities(m_normalizingIntensity));

      // double max_int = normalized_trace.maxYDataPoint().y;
      // qDebug() << "After normalization max int:" << max_int;
    }
  else
    m_finalTrace = m_mapTrace.toTrace();

  m_ui.resultPlainTextEdit->appendPlainText(m_finalTrace.toString());

  // Now switch to the page that contains these results:

  // Provide the title of the mass spectrum there for the user to have a last
  // opportunity to change it.

  m_ui.massSpectrumNameResultsLineEdit->setText(
    m_ui.massSpectrumTitleLineEdit->text());

  m_ui.tabWidget->setCurrentIndex(static_cast<int>(TabWidgetPage::RESULTS));
}


void
IsotopicClusterShaperDlg::outputFileName()
{
  m_fileName = QFileDialog::getSaveFileName(
    this, tr("Export to text file"), QDir::homePath(), tr("Any file type(*)"));
}


void
IsotopicClusterShaperDlg::displayMassSpectrum()
{
  // We must display the result in the mass spectrum-displaying window and
  // in all the pinned_down widgets.

  // We want that the process going on from now on has an unambiguous
  // file/sample name, so we craft the name here.

  // Finally, publish the mass spectrum.

  // qDebug() << "Going to write this trace:" << m_finalTrace.toString();

  QString title = craftMassSpectrumName();
  emit displayMassSpectrumSignal(
    title,
    m_colorByteArray,
    std::make_shared<const pappso::Trace>(m_finalTrace));


  // libmass::MassDataCborMassSpectrumHandler cbor_handler(thiss, m_finalTrace);
  // cbor_handler.setTitle(title);
  // QByteArray byte_array;
  // cbor_handler.writeByteArray(byte_array);
  // emit massDataToBeServedSignal(byte_array);

  // Here we need to somehow transfer the mass spectral data to a program
  // that can display that spectrum, typically minexpert2.
}


void
IsotopicClusterShaperDlg::copyMassSpectrumToClipboard()
{

  // Simply copy the results shown in the text edit widget to the clipboard.
  // We cannot use m_mapTrace for that because if normalization was asked for,
  // then we would not see that in m_mapTrace (normalization is local).

  QString trace_text = m_ui.resultPlainTextEdit->toPlainText();

  if(trace_text.isEmpty())
    {
      message("The mass spectrum is empty.");
      return;
    }

  QClipboard *clipboard = QApplication::clipboard();
  clipboard->setText(trace_text);
}


void
IsotopicClusterShaperDlg::traceColorPushButtonClicked()
{

  // Allow (true) the user to select a color that has been chosen already.
  QColor color = ColorSelector::chooseColor(true);

  if(!color.isValid())
    return;

  // Store the color as a QByteArray in the member datum.
  QDataStream stream(&m_colorByteArray, QIODevice::WriteOnly);
  stream << color;

  // Update the color of the button.
  QPushButton *colored_push_button = m_ui.colorSelectorPushButton;
  colorizePushButton(colored_push_button, m_colorByteArray);
}


void
IsotopicClusterShaperDlg::setIsotopiCluster(
  pappso::TraceCstSPtr isotopic_cluster_sp)
{
  // Start by copying the centroids locally in the form of a Trace collecting
  // DataPoint objects.
  m_isotopicCluster.assign(isotopic_cluster_sp->begin(),
                           isotopic_cluster_sp->end());

  // Now create the text to display that in the text edit widget.
  QString result_as_text;

  // Convert the data into a string that can be displayed in the text edit
  // widget.

  // Reset the value to min so that we can test for new intensities below.
  double greatestIntensity = std::numeric_limits<double>::min();

  for(auto data_point : m_isotopicCluster)
    {
      result_as_text += QString("%1 %2\n")
                          .arg(data_point.x, 0, 'f', 30)
                          .arg(data_point.y, 0, 'f', 30);

      // Store the peak centroid that has the greatest intensity.
      // Used to compute the FWHM value starting from resolution. And
      // vice-versa.
      if(data_point.y > greatestIntensity)
        {
          m_referencePeakMz = data_point.x;
          greatestIntensity = data_point.y;
        }
    }

  // qDebug() << "Reference (most intense) peak's m/z value:" <<
  // m_referencePeakMz;

  m_config.setReferencePeakMz(m_referencePeakMz);
  mp_massPeakShaperConfigWidget->setReferencePeakMz(m_referencePeakMz);

  m_ui.inputDataPointsPlainTextEdit->setPlainText(result_as_text);
}


void
IsotopicClusterShaperDlg::setMassSpectrumTitle(const QString &title)
{
  m_ui.massSpectrumTitleLineEdit->setText(title);
}


void
IsotopicClusterShaperDlg::setColorByteArray(const QByteArray &color_byte_array)
{
  m_colorByteArray = color_byte_array;

  // Update the color of the button.
  QPushButton *colored_push_button = m_ui.colorSelectorPushButton;
  colorizePushButton(colored_push_button, color_byte_array);
}

void
IsotopicClusterShaperDlg::colorizePushButton(QPushButton *push_button_p,
                                             const QByteArray &color_byte_array)
{
  QColor color;
  QDataStream stream(&m_colorByteArray, QIODevice::ReadOnly);
  stream >> color;

  if(color.isValid())
    {
      QPushButton *colored_push_button = m_ui.colorSelectorPushButton;

      QPalette palette = colored_push_button->palette();
      palette.setColor(QPalette::Button, color);
      colored_push_button->setAutoFillBackground(true);
      colored_push_button->setPalette(palette);
      colored_push_button->update();

      // Now prepare the color in the form of a QByteArray

      QDataStream stream(&m_colorByteArray, QIODevice::WriteOnly);

      stream << color;
    }
}

void
IsotopicClusterShaperDlg::importFromText()
{
  // We have peak centroids as in the text edit widget
  //
  // 59.032643588680002721957862377167 0.021811419157258506856811308694
  //
  // and we want to import them.

  QString peak_centroids_text =
    m_ui.inputDataPointsPlainTextEdit->toPlainText();

  // Easily transfom that into a Trace
  pappso::Trace trace(peak_centroids_text);

  pappso::TraceSPtr temp_cluster_sp = std::make_shared<pappso::Trace>();

  for(auto datapoint : trace)
    {
      temp_cluster_sp->append(datapoint);
    }

  // Finally do the real import.
  setIsotopiCluster(temp_cluster_sp);

  message("The data were imported. Please check the import results.");
}


void
IsotopicClusterShaperDlg::normalizingIntensityValueChanged()
{
  m_normalizingIntensity = m_ui.normalizingIntensitySpinBox->value();
}


void
IsotopicClusterShaperDlg::normalizingGrouBoxToggled(bool checked)
{
  if(!checked)
    m_normalizingIntensity = std::numeric_limits<double>::min();
  else
    m_normalizingIntensity = m_ui.normalizingIntensitySpinBox->value();
}


} // namespace libmassgui

} // namespace msxps
