/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

/////////////////////// Std lib includes

/////////////////////// Qt includes
#include <QDialog>
#include <QPushButton>
#include <QTimer>


/////////////////////// Local includes
#include "MassDataClientServerConfigDlg.hpp"


namespace msxps
{

namespace libmassgui
{


MassDataClientServerConfigDlg::MassDataClientServerConfigDlg(
  QWidget *program_window_p,
  const QString &applicationName,
  const QString &description)
  : QDialog(program_window_p),
    m_applicationName{applicationName},
    mp_programWindow(program_window_p)
{
  if(!program_window_p)
    qFatal("Programming error.");

  m_ui.setupUi(this);

  // Update the window title because the window title element in m_ui might be
  // either erroneous or empty.
  setWindowTitle(QString("%1 - %2").arg(applicationName).arg(description));

  //// We want to destroy the dialog when it is closed.
  // setAttribute(Qt::WA_DeleteOnClose);

  // Perform all the connections.

  // Typically, the user of startServerSignal is ProgramWindow that created this
  // dialog window.
  connect(m_ui.startServerPushButton, &QPushButton::clicked, this, [this]() {
    emit startServerSignal();
  });

  // Typically, the user of stopServerSignal is ProgramWindow that created this
  connect(m_ui.stopServerPushButton, &QPushButton::clicked, this, [this]() {
    emit stopServerSignal();
  });

  connect(m_ui.startClientPushButton,
          &QPushButton::clicked,
          this,
          &MassDataClientServerConfigDlg::startClient);

  connect(m_ui.stopClientPushButton, &QPushButton::clicked, this, [this]() {
    emit stopClientSignal();
  });

  connect(
    m_ui.connectionFrequencySlider, &QSlider::valueChanged, [this](int value) {
      message(QString("Set the connection frequency to %1 times per second")
                .arg(value));
    });
}


MassDataClientServerConfigDlg::~MassDataClientServerConfigDlg()
{
}


void
MassDataClientServerConfigDlg::closeEvent([[maybe_unused]] QCloseEvent *event)
{
  qDebug();

  hide();
}


void
MassDataClientServerConfigDlg::startClient()
{
  // We need to know what is the requested configuration.

  QString ip_address = m_ui.remoteServerIpAddressLineEdit->text();

  if(ip_address.isEmpty())
    {
      message("Please, configure the remote server IP address.");
      return;
    }

  QString port_number_string = m_ui.remoteServerPortNumberLineEdit->text();

  if(port_number_string.isEmpty())
    {
      message("Please, configure the remote server port number");
      return;
    }

  bool ok         = false;
  int port_number = port_number_string.toInt(&ok);

  if(!ok)
    {
      message(
        "Please, configure the remote server port number (that should be an "
        "integer)");
      return;
    }

  qDebug() << "Asking that a client be started to connect to a remote server:"
           << ip_address << ":" << port_number;

  int connection_frequency = m_ui.connectionFrequencySlider->value();

  // Typically the user of startClientSignal is ProgramWindow that created this
  // dialog window.
  emit startClientSignal(ip_address, port_number, connection_frequency);
}


void
MassDataClientServerConfigDlg::message(const QString &message, int timeout)
{
  if(m_ui.messageLineEdit->text() == message)
    return;

  m_ui.messageLineEdit->setText(message);
  QTimer::singleShot(timeout, [this]() { m_ui.messageLineEdit->setText(""); });
}


void
MassDataClientServerConfigDlg::updateClientConfigurationData(
  const QString &ip_address, int port_number)
{
  m_ui.localServerIpAddressLineEdit->setText(ip_address);
  m_ui.localServerPortNumberLineEdit->setText(QString::number(port_number));
}


void
MassDataClientServerConfigDlg::clientFailingFeedback(const QString &error)
{
  qDebug();
  message(error, 10000);
}

} // namespace libmassgui

} // namespace msxps

