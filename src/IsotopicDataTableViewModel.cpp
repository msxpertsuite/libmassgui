/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Std lib includes
#include <memory>


/////////////////////// Qt includes


/////////////////////// libmass includes
#include <libmass/Isotope.hpp>


/////////////////////// Local includes
#include "IsotopicDataTableViewModel.hpp"
#include "IsotopicClusterGeneratorDlg.hpp"


namespace msxps
{

namespace libmassgui
{


IsotopicDataTableViewModel::IsotopicDataTableViewModel(
  QObject *parent, libmass::IsotopicDataSPtr isotopic_data_sp)
  : QAbstractTableModel(parent), msp_isotopicData(isotopic_data_sp)
{
  if(!parent)
    qFatal("Programming error.");

  mp_parent = dynamic_cast<QWidget *>(parent);

  connect(this,
          &IsotopicDataTableViewModel::editCompletedSignal,
          this,
          &IsotopicDataTableViewModel::editCompleted);
}


IsotopicDataTableViewModel::~IsotopicDataTableViewModel()
{
}


const QWidget *
IsotopicDataTableViewModel::parent() const
{
  return mp_parent;
}


void
IsotopicDataTableViewModel::setTableView(IsotopicDataTableView *tableView)
{
  if(!tableView)
    qFatal("Programming error.");

  mp_tableView = tableView;
}


IsotopicDataTableView *
IsotopicDataTableViewModel::tableView()
{
  return mp_tableView;
}


int
IsotopicDataTableViewModel::rowCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent);

  return msp_isotopicData->size();
}


int
IsotopicDataTableViewModel::columnCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent);

  return static_cast<int>(libmass::IsotopeFields::LAST);
}


bool
IsotopicDataTableViewModel::insertRow(int row, const QModelIndex &parent)
{
  // The insertion occurs at index right *before* the row parameter.

  int first = row;
  int last  = first;

  qDebug() << "Preparing to insert before index:" << row;

  beginInsertRows(parent, first, last);

  libmass::IsotopeSPtr isotope_sp = std::make_shared<libmass::Isotope>(
    0, "NOT_SET", "NOT_SET", 0, 0.000000000, 0, 0, 1.00000000, 0, false);

  // false: do not update the mass maps as the newly inserted isotope is fake.
  // insert above row in the isotope vector.
  bool res = msp_isotopicData->insertNewIsotope(isotope_sp, row, false);

  endInsertRows();

  // Because we want to edit the newly inserted row, select it.
  mp_tableView->selectRow(row);

  emit modifiedSignal();

  return res;
}


QVariant
IsotopicDataTableViewModel::headerData(int section,
                                  Qt::Orientation orientation,
                                  int role) const
{
  if(role != Qt::DisplayRole)
    return QVariant();

  if(orientation == Qt::Vertical)
    {
      // Return the row number.
      QString valueString;
      valueString.setNum(section);
    }
  else if(orientation == Qt::Horizontal)
    {
      // Return the header of the column.
      switch(section)
        {
          case static_cast<int>(libmass::IsotopeFields::ID):
            return "Id";

          case static_cast<int>(libmass::IsotopeFields::ELEMENT):
            return "Element";

          case static_cast<int>(libmass::IsotopeFields::SYMBOL):
            return "Symbol";

          case static_cast<int>(libmass::IsotopeFields::ATOMIC_NUMBER):
            return "Atomic N°";

          case static_cast<int>(libmass::IsotopeFields::MASS):
            return "Mass";

          case static_cast<int>(libmass::IsotopeFields::MASS_NUMBER):
            return "Mass N°";

          case static_cast<int>(libmass::IsotopeFields::EXTRA_NEUTRONS):
            return "Extra neutrons";

          case static_cast<int>(libmass::IsotopeFields::PROBABILITY):
            return "Prob.";

          case static_cast<int>(libmass::IsotopeFields::LOG_PROBABILITY):
            return "Log prob.";

          case static_cast<int>(libmass::IsotopeFields::RADIOACTIVE):
            return "Radio.";
          default:
            qFatal("Programming error.");
        }
    }

  // Should never get here.
  return QVariant();
}


QVariant
IsotopicDataTableViewModel::data(const QModelIndex &index, int role) const
{
  if(!index.isValid())
    return QVariant();

  if(role == Qt::TextAlignmentRole)
    {
      return int(Qt::AlignRight | Qt::AlignVCenter);
    }
  else if(role == Qt::DisplayRole)
    {
      int row    = index.row();
      int column = index.column();

      // Let's get the data for the right column and the right
      // row. Let's find the row first, so that we get to the proper
      // entity.

      libmass::IsotopeSPtr isotope_sp = msp_isotopicData->getIsotopes().at(row);

      // Now see what's the column that is asked for. Prepare a
      // string that we'll feed with the right data before returning
      // it as a QVariant.

      QString valueString;

      if(column == static_cast<int>(libmass::IsotopeFields::ID))
        {
          valueString.setNum(isotope_sp->getId());
        }
      else if(column == static_cast<int>(libmass::IsotopeFields::ELEMENT))
        {
          valueString = isotope_sp->getElement();
        }
      else if(column == static_cast<int>(libmass::IsotopeFields::SYMBOL))
        {
          valueString = isotope_sp->getSymbol();
        }
      else if(column == static_cast<int>(libmass::IsotopeFields::ATOMIC_NUMBER))
        {
          valueString.setNum(isotope_sp->getAtomicNo());
        }
      else if(column == static_cast<int>(libmass::IsotopeFields::MASS))
        {
          valueString = QString("%1").arg(isotope_sp->getMass(), 0, 'f', 12);
        }
      else if(column == static_cast<int>(libmass::IsotopeFields::MASS_NUMBER))
        {
          valueString.setNum(isotope_sp->getMassNo());
        }
      else if(column ==
              static_cast<int>(libmass::IsotopeFields::EXTRA_NEUTRONS))
        {
          valueString.setNum(isotope_sp->getExtraNeutrons());
        }
      else if(column == static_cast<int>(libmass::IsotopeFields::PROBABILITY))
        {
          valueString =
            QString("%1").arg(isotope_sp->getProbability(), 0, 'f', 60);
        }
      else if(column ==
              static_cast<int>(libmass::IsotopeFields::LOG_PROBABILITY))
        {
          valueString =
            QString("%1").arg(isotope_sp->getLogProbability(), 0, 'f', 60);
        }
      else if(column == static_cast<int>(libmass::IsotopeFields::RADIOACTIVE))
        {
          valueString = isotope_sp->getRadioactive() ? "true" : "false";
        }
      else
        {
          qFatal("Programming error.");
        }

      return valueString;
    }
  // End of
  // else if(role == Qt::DisplayRole)

  return QVariant();
}


bool
IsotopicDataTableViewModel::setData(const QModelIndex &index,
                               const QVariant &value,
                               int role)
{
  if(role != Qt::EditRole)
    return false;

  if(!checkIndex(index))
    return false;

  // We want to access the isotope in the isotopic data. This is done by using
  // the index, that is, the row.

  int isotopic_data_index = index.row();
  int column              = index.column();

  // Store the original isotope pointer!
  libmass::IsotopeSPtr old_isotope_sp =
    msp_isotopicData->getIsotopes().at(isotopic_data_index);

  // Prepare a copy that we'll modify the proper field of.
  libmass::IsotopeSPtr new_isotope_sp =
    std::make_shared<libmass::Isotope>(*old_isotope_sp);

  // Now determine what is the column of the cell (that is, the field) that was
  // modified and modify the new_isotope_sp accordingly.

  bool ok = false;

  if(column == static_cast<int>(libmass::IsotopeFields::ID))
    {
      int new_value = value.toInt(&ok);
      if(!ok)
        return false;

      new_isotope_sp->setId(new_value);
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::ELEMENT))
    {
      new_isotope_sp->setElement(value.toString());
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::SYMBOL))
    {
      new_isotope_sp->setSymbol(value.toString());
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::ATOMIC_NUMBER))
    {
      int new_value = value.toInt(&ok);
      if(!ok)
        return false;

      new_isotope_sp->setAtomicNo(new_value);
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::MASS))
    {
      double new_value = value.toDouble(&ok);
      if(!ok)
        return false;

      new_isotope_sp->setMass(new_value);
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::MASS_NUMBER))
    {
      int new_value = value.toInt(&ok);
      if(!ok)
        return false;

      new_isotope_sp->setMassNo(new_value);
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::EXTRA_NEUTRONS))
    {
      int new_value = value.toInt(&ok);
      if(!ok)
        return false;

      new_isotope_sp->setExtraNeutrons(new_value);
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::PROBABILITY))
    {
      double new_value = value.toDouble(&ok);
      if(!ok)
        return false;

      new_isotope_sp->setProbability(new_value);

      // How about computing the prob log? We could emit the right signal for
      // the user of this model to make profit of it. Attention, ths is the
      // ln(), not log10!

      new_isotope_sp->setLogProbability(std::log(new_value));
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::LOG_PROBABILITY))
    {
      double new_value = value.toDouble(&ok);
      if(!ok)
        return false;

      new_isotope_sp->setLogProbability(new_value);

      // How about computing the prob? Attention, ths is the exp() not the
      // 10^()!
      //new_isotope_sp->setProbability(std::exp(new_value));
    }
  else if(column == static_cast<int>(libmass::IsotopeFields::RADIOACTIVE))
    {
      bool new_value = value.toBool();

      new_isotope_sp->setRadioactive(new_value);
    }
  else
    {
      qFatal("Programming error.");
    }

  // At this point we know the new isotope has fine data in it, just replace the
  // old with the new.

  msp_isotopicData->replace(old_isotope_sp, new_isotope_sp);

  // emit editCompleted(new_isotope_sp->toString());

  // Not sure if this is really necessary.
  emit(dataChanged(index, index));

  emit modifiedSignal();

  return true;
}


void
IsotopicDataTableViewModel::refreshAfterNewData()
{
  // This is bad code to ensure that the table view items are refreshed
  // throughout all the data set.
  QModelIndex parent;

  beginInsertRows(parent, 0, rowCount() - 1);

  endInsertRows();

  emit modifiedSignal();
}


bool
IsotopicDataTableViewModel::clearAllData()
{
  // How many rows do we have?

  int row_count = rowCount();

  if(!row_count)
    // We did not remove any Isotope from the IsotopicData
    return false;

  // The begin_index and end_index are *inclusive* in the beginRemoveRows()
  // call below.
  beginRemoveRows(QModelIndex(), 0, row_count - 1);

  // true: update the maps because we need to clear all the items there.
  msp_isotopicData->eraseIsotopes(0, row_count - 1, true);

  endRemoveRows();

  emit modifiedSignal();

  // We did remove items.
  return true;
}


Qt::ItemFlags
IsotopicDataTableViewModel::flags(const QModelIndex &index) const
{
  return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}


void
IsotopicDataTableViewModel::editCompleted(const QString &text)
{
  qDebug() << "After editing the new isotope:" << text;

  emit modifiedSignal();
}


void
IsotopicDataTableViewModel::insertIsotopeRowAbove()
{
  // We need to maintain a perfect correlation between the data in the
  // IsotopicData object and the rows in the table view.

  int insertion_row_index = 0;

  // We want to add a row above the current index. Note that all the functions
  // dealing with models and containers insert above the index provided to the
  // insertion function.

  int row_count = rowCount();

  if(!row_count)
    {
      // qDebug() << "There are no isotopes.";

      // We do not care, because we cannot, if there is a current index.
    }
  else
    {
      // qDebug() << "There are currently" << row_count << "isotopes in the
      // table";

      // In this case, we want to know if there is a current index.

      QItemSelectionModel *selection_model = mp_tableView->selectionModel();

      QModelIndex current_index = selection_model->currentIndex();

      if(!current_index.isValid())
        {
          return;
        }

      insertion_row_index = current_index.row();

      // qDebug() << "The current index row is at index" << insertion_row_index;
    }

  // insertRow() inserts a single row before the given row in the child items of
  // the parent specified. This means that we just use that row index without
  // modifying it.

  insertRow(insertion_row_index);

  emit modifiedSignal();
}


void
IsotopicDataTableViewModel::insertIsotopeRowBelow()
{
  // We need to maintain a perfect correlation between the data in the
  // IsotopicData object and the rows in the table view.

  int insertion_row_index = 0;

  // We want to add a row below the current index, but beware that all the
  // functions dealing with models and containers insert *above* the index
  // provided to the insertion function.

  int row_count = rowCount();

  if(!row_count)
    {
      // qDebug() << "There are no isotopes.";

      // We do not care, because we cannot, if there is a current index.
    }
  else
    {
      // qDebug() << "There are currently" << row_count << "isotopes in the
      // table";

      // In this case, we want to know if there is a current index.

      QItemSelectionModel *selection_model = mp_tableView->selectionModel();

      QModelIndex current_index = selection_model->currentIndex();

      if(!current_index.isValid())
        {
          return;
        }

      insertion_row_index = current_index.row();

      // qDebug() << "The current index row is at index" << insertion_row_index;
    }

  // insertRow() inserts a single row before the given row in the child items of
  // the parent specified. This means that we'll have to increment the
  // insertion_row_index by 1.

  ++insertion_row_index;

  insertRow(insertion_row_index);

  emit modifiedSignal();
}


void
IsotopicDataTableViewModel::removeSelected()
{
  // We need to maintain a perfect correlation between the data in the
  // IsotopicData object and the rows in the table view.

  // int row_count = rowCount();
  // qDebug() << "There are currently" << row_count
  //<< "isotopes in the table view model.";

  QItemSelectionModel *selection_model = mp_tableView->selectionModel();

  QModelIndexList selected_indices = selection_model->selectedIndexes();

  // qDebug() << "The number of selected indices:" << selected_indices.size();

  // We know that the indices list might contain multiple indices for the same
  // row. We need to extract a unique list of rows for which at least one index
  // is selected. Then, we'll use that list of rows to perform the removal.

  std::set<int> selected_rows;

  for(int iter = 0; iter < selected_indices.size(); ++iter)
    {
      selected_rows.insert(selected_indices.at(iter).row());
    }

  if(!selected_rows.size())
    {
      // qDebug() << "There are not selected rows in the table view.";
      return;
    }

  int begin_index = *selected_rows.begin();

  // The end index is at position end() - 1 of the std::set.
  int end_index = *std::prev(selected_rows.end());

  // qDebug() << "The rows to be removed are in the fully inclusive index range:
  // ["
  //<< begin_index << "-" << end_index << "].";

  // The begin_index and end_index are *inclusive* in the beginRemoveRows()
  // call below.
  beginRemoveRows(QModelIndex(), begin_index, end_index);

  // true: update the maps because the remaining isotopes are valid.
  msp_isotopicData->eraseIsotopes(begin_index, end_index, true);

  endRemoveRows();

  emit modifiedSignal();

  // Now select the item that was right below the last selected removed item.
  if(!rowCount())
    {
      // Do nothing.
    }
  else
    {
      if(begin_index >= rowCount())
        {
          begin_index = rowCount() - 1;
        }
    }

  mp_tableView->selectRow(begin_index);
}


} // namespace libmassgui

} // namespace msxps
